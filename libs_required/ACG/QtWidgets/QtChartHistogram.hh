/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2024, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/


//=============================================================================
//
//  CLASS QtChart - IMPLEMENTATION
//
//=============================================================================

//== INCLUDES =================================================================
#include <QChart>
#include <QBarSeries>
#include <QLineSeries>
#include <QStackedBarSeries>
#include <QChartView>
#include <QRubberBand>
#include <cfloat>
#include <QValueAxis>


#include <QBarSet>



//== NAMESPACES ===============================================================
namespace ACG {
namespace QtWidgets {

//This class creates a Histogram in a QChartView. The Histogram can be zoomed in and out by using a rubberband. 
//The x-axis values are updated when the rubberband is moved. The y-axis values are updated automatically.
//The Histogram can be color coded.
//!!!!!IMPORTANT!!!! This class allows for colored bars in the Histogram, which is not possible with QCharts alone. 
//In order to achieve that we have to create a new QBarSet for each bar. And "stack" them on top of each other.
//This is not very efficient and should be avoided if possible. In addition to that, the x-axis is detached in order to allow 
//a correct zoom behavior. (This is also very hacky and should be avoided but I didnt see any other way to make it work)
//However I think there is a bug in the QCharts library and as soon as it is fixed, the workaround is not needed anymore.
class QtChartHistogram : public QChartView {
    Q_OBJECT

public:

    /// Default constructor
    explicit QtChartHistogram(QString title_X, QString title_Y, const std::vector<double>& _values, QWidget* _parent = 0);

    /// Destructor
    ~QtChartHistogram();

    void setColorCoding(bool _colorCoding);

    void setValues(const std::vector<double>& _values);

    void replot();

public slots:

  void rubberBandChangedSlot(double rubber_min, double rubber_max);

signals:  

  void rubberBandChanged(double rubber_min, double rubber_max);

private:

    //variables
    QChart* chart_;
    QValueAxis* axisX;
    QValueAxis* axisY;
    QStackedBarSeries* series;
    QRubberBand* rubberBand_;
    bool colorCoding_;
    std::vector<double> values_;

protected:

    bool eventFilter(QObject *obj, QEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

};

} // namespace QtWidgets
} // namespace ACG






import os, shutil, subprocess, sys

print("-----------------------")
print("Running tests via run_tests.py for "+sys.platform)

ctest_executable = ""

print("===============================================")
print("Python test runner script settings")
print("===============================================")

print("Building with:")

def getStringCheckEnv( variablename  ):
   environmentvariable = os.environ.get(variablename)
   if environmentvariable is None:
      return str('Unset')
   else:
      return environmentvariable


# setup operating system
if sys.platform == "linux" or sys.platform == "linux2":
    ctest_executable = "ctest"

    

    print("COMPILER        : " +getStringCheckEnv('COMPILER'))
    print("LANGUAGE        : " +getStringCheckEnv('LANGUAGE'))
    print("QTVERSION       : " +getStringCheckEnv('QTVERSION'))
    print("BUILDTYPE       : " +getStringCheckEnv('BUILDTYPE'))
    print("PYTHON          : " +getStringCheckEnv('PYTHON'))

    print("===============================================")
    print("===============================================")

    print(" ")
    
    print("===============================================")

    print("OpenFlipper link information")
    print("===============================================")

    print("Current working directory for check: " + os.getcwd())

    openflipper_executable = os.path.join(os.getcwd(),"..","Build","bin","OpenFlipper")
    
    output = subprocess.check_output( [ "ldd", openflipper_executable])
    
    print(str(output))


elif sys.platform == "darwin":
    if os.path.exists("/opt/local/bin/ctest"):
       ctest_executable = os.path.join("/","opt", "local", "bin", "ctest")
    if os.path.exists("/usr/local/bin/ctest"):
       ctest_executable = os.path.join("/","usr", "local", "bin", "ctest")
elif sys.platform == "win32":
    ctest_executable = os.path.join("C:","Program Files","CMake","bin","ctest")
    cmake_executable = os.path.join("C:","Program Files","CMake","bin","cmake")


    print("ARCHITECTURE        : " +getStringCheckEnv('ARCHITECTURE'))
    print("BUILD_PLATFORM      : " +getStringCheckEnv('BUILD_PLATFORM'))
    print("GTESTVERSION        : " +getStringCheckEnv('GTESTVERSION'))
    print("GENERATOR           : " +getStringCheckEnv('GENERATOR'))
    print("VS_PATH             : " +getStringCheckEnv('VS_PATH'))
    if "LIBPATH" in os.environ:
      print("LIBPATH             : " +getStringCheckEnv('LIBPATH'))
    else:
      print("LIBPATH             : UNSET" )

    if "LIBPATH_BASE" in os.environ:
      print("LIBPATH_BASE        : " +getStringCheckEnv('LIBPATH_BASE'))
    else:
      print("LIBPATH_BASE        : UNSET")
  
    if "QT_INSTALL_PATH" in os.environ:
      print("QT_INSTALL_PATH     : " +getStringCheckEnv('QT_INSTALL_PATH'))
    else:
      print("QT_INSTALL_PATH     : UNSET")

    print("CMAKE_CONFIGURATION :" +getStringCheckEnv('CMAKE_CONFIGURATION'))
    print("===============================================")
    print("===============================================")
    print("")
    print("===============================================")
    print("")
    print("")
    print("Running Build environment checks")
    print("Current working directory: " + os.getcwd( ))

    
    if os.path.exists(getStringCheckEnv('LIBPATH_BASE')):
      print("LIBPATH_BASE " +getStringCheckEnv('LIBPATH_BASE') + " is ok")
    else:
      print("ERROR: LIBPATH_BASE " +getStringCheckEnv('LIBPATH_BASE') + " not found!")
      exit(10)

    if "QT_INSTALL_PATH" in os.environ:
       if os.path.exists(getStringCheckEnv('QT_INSTALL_PATH')):
         print("QT_INSTALL_PATH " +getStringCheckEnv('QT_INSTALL_PATH') + " is ok")
       else:
         print("ERROR: QT_INSTALL_PATH " +getStringCheckEnv('QT_INSTALL_PATH') + " not found!")
         exit(10)

    print("Entering rel directory")
    os.chdir(os.path.join(os.path.abspath(os.path.curdir),u'rel')) 
    print("Current working directory: " + os.getcwd( ))

    if os.path.exists("CMakeCache.txt"):
        print("Remove old CMakeCache.txt")
        os.remove("CMakeCache.txt")

    if os.path.exists("CTestTestfile.cmake"):
        print("Remove old CTestTestfile.cmake")
        os.remove("CTestTestfile.cmake")

    if os.path.exists("DartConfiguration.tcl"):
        print("Remove old DartConfiguration.tcl")
        os.remove("DartConfiguration.tcl")

    print("", flush=True)

    gtest_prefix        = "-DGTEST_PREFIX=\"" +getStringCheckEnv('LIBPATH_BASE')+"\\" +getStringCheckEnv('ARCHITECTURE') + "\\" + getStringCheckEnv('GTESTVERSION') + "\""
    cmake_generator     =getStringCheckEnv('GENERATOR')
    cmake_configuration_list =getStringCheckEnv('CMAKE_CONFIGURATION').split()

    print(cmake_configuration_list)

    cmake_parameters = [cmake_executable, gtest_prefix , "-G", cmake_generator ,"-DCMAKE_BUILD_TYPE=Release","-DOPENFLIPPER_BUILD_UNIT_TESTS=TRUE" ]

    cmake_parameters.extend(cmake_configuration_list)
    cmake_parameters.append("..")

    print(cmake_parameters)
 
    cmake_call = subprocess.run( cmake_parameters )

    print("Cmake call info: ")
    print(cmake_call)
    print("Return Code: ", cmake_call.returncode)

    if cmake_call.returncode != 0:
       print("CMake error! exiting")
       exit(cmake_call.returncode)

    print("Entering tests directory")
    os.chdir(os.path.join(os.path.abspath(os.path.curdir),u'tests')) 
    print("Current working directory: " + os.getcwd( ))

    # preparing DLLs to make sure we can run the application
    if not os.path.exists("../Build"):
       message("Error: the folder ../Build has not been found!")
       exit(1)

    src_files = os.listdir("../Build")
    for file_name in src_files:
        full_file_name = os.path.join("..\Build", file_name)
        if os.path.isfile(full_file_name) and full_file_name.endswith(".dll"):
           print("Copyining" + full_file_name + " to testBinaries")
           shutil.copy(full_file_name, "testBinaries")




os.chdir(os.path.join(os.getcwd(), os.path.pardir))  # cd ..
print("Working directory for test cleanup: " + os.getcwd())

try:
    shutil.rmtree(os.path.join(os.getcwd(), "tests", "testResults")) # delete test results if exists
except FileNotFoundError :
    print("testResults not found")

try:
    os.remove(os.path.join(os.getcwd(), "CTestResults.xml")) # delete test results if exists
except FileNotFoundError:
    print("CTestResults not found")

print("Creating dir for test results")
os.mkdir(os.path.join(os.getcwd(), "tests", "testResults"))

print("Working directory for test execution: " + os.getcwd(), flush=True)

# call ctest
ctest = subprocess.run(
    [ctest_executable, "-C", "Debug", "--no-tests=error", "--no-compress-output", "--output-on-failure","--output-junit","report.xml"])

print("Return Code: ", ctest.returncode)

if not os.path.exists(os.path.join(os.getcwd(),"report.xml")):
  print("No report found! Directory content:")
  dirfiles = os.listdir(".")
  print(dirfiles)
else:
  # copy to a more suitable location for pickup as artifact
  shutil.copyfile(os.path.join(os.getcwd(),"report.xml"),os.path.join(os.getcwd(),"../","../","report.xml") )

print("-----------------------")

sys.exit(ctest.returncode)

# - Try to find Teem
# Once done this will define
#  
#  TEEM_FOUND        - system has Teem
#  TEEM_INCLUDE_DIR  - the Teem include directory
#  TEEM_LIBRARY      - Link these to use Teem
#   

IF(TEEM_INCLUDE_DIR)
  # Already in cache, be silent
  SET(TEEM_FIND_QUIETLY TRUE)
ENDIF(TEEM_INCLUDE_DIR)

if(WIN32)
   FIND_PATH(TEEM_INCLUDE_DIR unrrdu.h nrrdDefines.h
              PATHS "c:/teem/include" "c:/teem/teem-build/include")
   SET(TEEM_NAMES teem)
   FIND_LIBRARY(TEEM_LIBRARY
                 NAMES ${TEEM_NAMES}
                 PATHS "c:/teem/bin"  "c:/teem/teem-build/bin")
else(WIN32)
   FIND_PATH(TEEM_INCLUDE_DIR unrrdu.h nrrdDefines.h
              PATHS /usr/local/include /usr/include)
   SET(TEEM_NAMES teem TEEM)
   FIND_LIBRARY(TEEM_LIBRARY
                 NAMES ${TEEM_NAMES}
                 PATHS /usr/lib /usr/local/lib)
endif(WIN32)

GET_FILENAME_COMPONENT(TEEM_LIBRARY_DIR ${TEEM_LIBRARY} PATH)

IF(TEEM_INCLUDE_DIR AND TEEM_LIBRARY)
   SET(TEEM_FOUND TRUE)
ELSE(TEEM_INCLUDE_DIR AND TEEM_LIBRARY)
   SET(TEEM_FOUND FALSE)
   SET(TEEM_LIBRARY_DIR)
ENDIF(TEEM_INCLUDE_DIR AND TEEM_LIBRARY)


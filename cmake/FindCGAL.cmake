GET_FILENAME_COMPONENT(module_file_path ${CMAKE_CURRENT_LIST_FILE} PATH )

# Check if the base path is set
if ( NOT CMAKE_WINDOWS_LIBS_DIR )
  # This is the base directory for windows library search used in the finders we shipp.
  set(CMAKE_WINDOWS_LIBS_DIR "c:/libs" CACHE STRING "Default Library search dir on windows." )
endif()

if ( CMAKE_GENERATOR MATCHES "^Visual Studio 11.*Win64" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2012/x64/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 11.*" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2012/x32/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 12.*Win64" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2013/x64/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 12.*" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2013/x32/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 14.*Win64" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2015/x64/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 14.*" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2015/x32/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 15.*Win64" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2017/x64/")
elseif ( CMAKE_GENERATOR MATCHES "^Visual Studio 15.*" )
  SET(VS_SEARCH_PATH "${CMAKE_WINDOWS_LIBS_DIR}/vs2017/x32/")
endif()

# The CGAL cmake files pollute our CMAKE_MODULE_PATH, so it can
# happen that we accidentally use their own old-school finders
set(CMAKE_MODULE_PATH_BACKUP ${CMAKE_MODULE_PATH})

FIND_PACKAGE( CGAL REQUIRED CONFIG
              HINTS $ENV{CGAL_DIR}
                    "${VS_SEARCH_PATH}/CGAL-4.13"
                    "${VS_SEARCH_PATH}/CGAL-4.12"
                    /usr/
                    ../../External
                    ${module_file_path}/../../../External
              PATH_SUFFIXES lib lib/cmake
              NO_CMAKE_PATH              # if CMAKE_PREFIX_PATH is set we find CGAL 4.12 instead of 4.13. I dont know why that is.
              NO_CMAKE_ENVIRONMENT_PATH) # Set NO_CMAKE_PATH and NO_CMAKE_ENVIRONMENT_PATH to ignore CMAKE_PREFIX_PATH

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH_BACKUP})

if (CGAL_FOUND)
  set(CGAL_BIN_DIRS ${CGAL_BIN_DIR} ${CGAL_BIN_DIR}/../auxiliary/gmp/lib)
endif()

if (NOT TARGET CGAL::CGAL)
  message(FATAL_ERROR "Your CGAL version does not define the CGAL::CGAL target which probably means it is too old")
endif()

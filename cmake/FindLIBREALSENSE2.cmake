# - Try to find LIBREALSENSE2
# Once done this will define
#  LIBREALSENSE2_FOUND - System has LIBREALSENSE2
#  LIBREALSENSE2_INCLUDE_DIRS - The LIBREALSENSE2 include directories
#  LIBREALSENSE2_LIBRARIES - The libraries needed to use LIBREALSENSE2
#  LIBREALSENSE2_DEFINITIONS - Compiler switches required for using LIBREALSENSE2

find_package(PkgConfig)

find_path(LIBREALSENSE2_INCLUDE_DIR librealsense2/rs.hpp
          HINTS "C:/Program Files (x86)/Intel RealSense SDK 2.0/include"
          PATH_SUFFIXES LIBREALSENSE2 )  

find_library(LIBREALSENSE2_LIBRARY NAMES realsense2
             HINTS "C:/Program Files (x86)/Intel RealSense SDK 2.0/lib/x64" )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBREALSENSE2_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LIBREALSENSE2  DEFAULT_MSG
                                  LIBREALSENSE2_LIBRARY LIBREALSENSE2_INCLUDE_DIR)

mark_as_advanced(LIBREALSENSE2_INCLUDE_DIR LIBREALSENSE2_LIBRARY )

set(LIBREALSENSE2_LIBRARIES ${LIBREALSENSE2_LIBRARY} )
set(LIBREALSENSE2_INCLUDE_DIRS ${LIBREALSENSE2_INCLUDE_DIR} )



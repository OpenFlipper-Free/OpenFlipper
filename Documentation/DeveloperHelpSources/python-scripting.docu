/*!
\page python_scripting OpenFlipper Python Scripting and Batch Mode

  \section python_scripting_batch_mode OpenFlipper Batch mode with python
    OpenFlipper can be started in batch mode without a graphical user interface by providing "-b" 
    as a command line option. Furthermore you have to provide an OpenFlipper python script (extension is .ofp),
    which will get executed. To get output to the command line, you also have to provide the command 
    line switch "-c". Only plugins which support batch mode ( They implement an nogui() function ), will
    be loaded. For example, no renderers, postprocessors or other plugins providing only user interface
    or graphical functionality will be loaded. You can see on the command line (when "-c" is given),
    which plugins are activated and which are skipped in batch mode.

    
  
  \section python_scripting_datattypes DataTypes
    \subsection  python_scripting_matrixtype Matrix4x4 data type
      The Matrix4x4T type used in the C++ code is mapped from python. Details can be found here:
      \subpage python_scripting_matrix4x4_type
  
    \subsection  python_scripting_vectortype 3 Dimensional vector type
      The Vector type used in the C++ code is mapped from python. Details can be found here:
      \subpage python_scripting_vector_type
  
  \subsection  python_scripting_idlisttype IDList data type
      The IdList type used in the C++ code is mapped from python. Details can be found here:
      \subpage python_scripting_IDList_type

  \section  python_scripting_examples Examples

   Here is a simple example of a python script using the holefiller:
   \code
      # Load an object from a specific path
      id = fileoff.loadObject("OpenFlipper-Free/OpenFlipper/cube1.off")

      # Prints the id of the new object
      print("ID of new object is : " + str(id) )

      # Selects a set of faces
      meshobjectselection.selectFaces(id,[0,1,2,3,4,5,6,7,8,9,10])

      # deletes the selected faces
      meshobjectselection.deleteFaceSelection(id)

      # And runs the hole filler to close the holes produced by the previous operation
      holefiller.fillAllHoles(id)
   \endcode

   Another example produces an animation of a rotating object:
   \code
     # We need numpy for array and vectors
     import numpy as np

     # Generate a cube via the primitivesgenerator plugin
     primitivesgenerator.addCube()

     # Set our animation parameters
     animation_axis = np.array([-1.0,1.0,0.0]);
     rotation_center = np.array([0.0,0.0,0.0])

     # Loop over 360 degrees
     for angle in range(0,360,1):
       # Apply rotation
       viewcontrol.rotate(animation_axis,1,rotation_center,0)

       # Update the current view
       core.updateView()

       # Update the ui
       core.updateUI()
   \endcode

  
 */

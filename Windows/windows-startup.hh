
#include "StackWalker/StackWalker.hh"

/* ==========================================================
 *
 * Stackwalker code. Used to get a backtrace if OpenFlipper
 * crashes under windows
 *
 * ==========================================================*/


class StackWalkerToConsole : public StackWalker
{
protected:
    virtual void OnOutput(LPCSTR szText) override;
};


/* ==========================================================
 *
 * Console for Windows to get additional output written via
 * cerr, cout, ... that is not forwarded to log window
 *
 * ==========================================================*/

void connect_console();
void attachConsole();


#include "windows-startup.hh"
#include <windows.h>
#include <errhandlingapi.h>
#include <fstream>
#include <iostream>
#include <OpenFlipper/common/GlobalOptions.hh>

/* ==========================================================
*
* Stackwalker code. Used to get a backtrace if OpenFlipper
* crashes under windows
*
* ==========================================================*/

void StackWalkerToConsole::OnOutput(LPCSTR szText) 
{
    // Writes crash dump to .OpenFlipper config directory
    std::ofstream crashFile;
    QString crashName = OpenFlipper::Options::configDirStr() + QDir::separator() + "CrashDump.txt";
    crashFile.open(crashName.toLatin1(),std::ios::out | std::ios::app);
    crashFile << szText;
    crashFile.close();

    // Write crash dump to console as well
    StackWalker::OnOutput(szText);
}



/* ==========================================================
 *
 * Console for Windows to get additional output written via
 * cerr, cout, ... that is not forwarded to log window
 *
 * ==========================================================*/

void connect_console()
{
  FILE* check = freopen("CONIN$", "r", stdin);
  if (check) {
    std::cerr << "Error reopening stdin" << std::endl;
  }
  check = freopen("CONOUT$", "w", stdout);
  if (check) {
    std::cerr << "Error reopening stdout" << std::endl;
  }
  check = freopen("CONOUT$", "w", stderr);
  if (check) {
    std::cerr << "Error reopening stderr" << std::endl;
  }
  std::cout.clear();
  std::cerr.clear();
  std::cin.clear();
  std::wcout.clear();
  std::wcerr.clear();
  std::wcin.clear();
}

  void attachConsole()
   {
     // try to attach the console of the parent process
     if (AttachConsole(-1))
     {
       // if the console was attached change stdinput and output
       connect_console();
     }
     else
     {
       // create and attach a new console if needed
 #ifndef NDEBUG
       // always open a console in debug mode
       AllocConsole();     
       connect_console();

       return;
 #endif
       if (OpenFlipper::Options::logToConsole())
       {
         AllocConsole();
         connect_console();
       }
     }
   }



/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/





// Mainwindow

#include "OpenFlipper/Core/Core.hh"
#include "common/glew_wrappers.hh"
#include "OpenFlipper/BasePlugin/PluginFunctionsCore.hh"

// Qt
#if QT_VERSION_MAJOR < 6
#include <qgl.h>
#endif

// stdc++
#include <csignal>
#include <regex>

#include <QCommandLineParser>

#if ( defined(WIN32))
  #define NO_EXECINFO
#endif

#ifndef NO_EXECINFO
#include <execinfo.h>
#endif

#ifdef PYTHON_ENABLED
  #include <PythonInterpreter/PythonInterpreter.hh>
#endif

#ifdef USE_OPENMP
#endif

#ifdef WIN32
  #include <Windows/windows-startup.hh>
#endif

#include <CommandLine/CommandLineParser.hh>


/* ==========================================================
 *
 * Linux function printing a full stack trace to the console
 *
 * ==========================================================*/
#ifndef NO_EXECINFO

#if defined(__GLIBCXX__) || defined(__GLIBCPP__)
// GCC: implement demangling using cxxabi
#include <cxxabi.h>
std::string demangle(const std::string& _symbol)
{
    int status;
    char* demangled = abi::__cxa_demangle(_symbol.c_str(), nullptr, nullptr, &status);
    if (demangled) {
        std::string result{demangled};
        free(demangled);
        if (status == 0) {
            return result;
        }
        else {
            return _symbol;
        }
    }
    else {
        return _symbol;
    }
}
#else
// other compiler environment: no demangling
std::string demangle(const std::string& _symbol)
{
    return _symbol;
}
#endif

void backtrace()
{
    void *addresses[20];
    char **strings;

    int size = backtrace(addresses, 20);
    strings = backtrace_symbols(addresses, size);
    std::cerr << "Stack frames: " << size << std::endl;
    // line format:
    // <path>(<mangled_name>+<offset>) [<address>]
    std::regex line_format{R"(^\s*(.+)\((([^()]+)?\+(0x[0-9a-f]+))?\)\s+\[(0x[0-9a-f]+)\]\s*$)"};
    for(int i = 0; i < size; i++) {
        std::string line{strings[i]};
        std::smatch match;
        std::regex_match(line, match, line_format);
        if (!match.empty()) {
            auto file_name = match[1].str();
            auto symbol = demangle(match[3].str());
            auto offset = match[4].str();
            auto address = match[5].str();
            std::cerr << i << ":";
            if (!file_name.empty()) std::cerr << " " << file_name << " ::";
            if (!symbol.empty()) std::cerr << " " << symbol;
            if (!offset.empty()) std::cerr << " (+" << offset << ")";
            if (!address.empty()) std::cerr << " [" << address << "]";
            std::cerr << std::endl;
        }
    }
    free(strings);
}
#endif

/* ==========================================================
 *
 * General segfault handler. This function is called if OpenFlipper
 * crashes
 *
 * ==========================================================*/
void segfaultHandling (int) {

  // prevent infinite recursion if segfaultHandling() causes another segfault
  std::signal(SIGSEGV, SIG_DFL);


  std::cerr << "\n" << std::endl;
  std::cerr << "\n" << std::endl;
  std::cerr << "\33[31m" << "=====================================================" << std::endl;
  std::cerr << "\33[31m" << "OpenFlipper or one of its plugins caused a Segfault." << std::endl;
  std::cerr << "\33[31m" << "This should not happen,... Sorry :-(" << std::endl;
  std::cerr << "\33[31m" << "=====================================================" << std::endl;
  std::cerr << "\n" << std::endl;

  // Linux Handler
#ifndef NO_EXECINFO
  std::cerr << "\33[0m"  << "Trying a backtrace to show what happened last: " << std::endl;
  backtrace();

  std::cerr << "\n" << std::endl;
  std::cerr << "Backtrace completed, trying to abort now ..." << std::endl;
#endif

  // Windows handler via StackWalker
#ifdef WIN32
  StackWalkerToConsole sw;
  sw.ShowCallstack();
#endif


  std::cerr << "Trying to get additional information (This might fail if the memory is corrupted)." << std::endl;

  if (OpenFlipper::Options::gui()) {
    for ( unsigned int i = 0 ; i <  4 ; ++i) {
      std::cerr << "DrawMode Viewer "<<  i << " " << PluginFunctions::drawMode(i).description() << std::endl;
    }
  }

  std::abort();
}


// Detector for OpenGL
#include "OpenGL/OpenGLDetection.hh"

int main(int argc, char **argv)
{
#ifdef _WIN32
  // This make crashes visible - without them, starting the
  // application from cmd.exe or powershell can surprisingly hide
  // any signs of a an application crash!
  SetErrorMode(0); // 0: Use the system default, which is to display all error dialog boxes.
#endif


  // Remove -psn_0_xxxxx argument which is automatically
  // attached by MacOSX
  for (int i = 0; i < argc; i++) {
    if(strlen(argv[i]) > 4) {
      if( ( (argv[i])[0] == '-' ) &&
        ( (argv[i])[1] == 'p' ) &&
        ( (argv[i])[2] == 's' ) &&
        ( (argv[i])[3] == 'n' ) ) {
        argc--;
        argv[i] = (char *)"";
      }
    }
  }

  OpenFlipper::Options::argc(&argc);
  OpenFlipper::Options::argv(&argv);

  // Set organization and application names
  QCoreApplication::setOrganizationName("VCI");
  QCoreApplication::setApplicationName(TOSTRING(PRODUCT_STRING));
  QCoreApplication::setApplicationVersion(OpenFlipper::Options::coreVersion());

  // initialize a core application to check for commandline parameters
  QCoreApplication* coreApp = new QCoreApplication(argc, argv);

  OpenFlipper::Options::initializeSettings();

  QCommandLineParser parser;
  QString errorMessage;

  // parse command line options
  switch (parseCommandLine(parser, &errorMessage)) {
  case CommandLineOk:
	  break;
  case CommandLineError:
	  fputs(qPrintable(errorMessage), stderr);
	  fputs("\n\n", stderr);
	  fputs(qPrintable(parser.helpText()), stderr);
	  return 1;
  case CommandLineVersionRequested:
	  printf("%s %s\n", qPrintable(QCoreApplication::applicationName()),
		  qPrintable(QCoreApplication::applicationVersion()));
	  return 0;
  case CommandLineHelpRequested:
	  parser.showHelp();
	  Q_UNREACHABLE();
  }

  // only one application is allowed so delete the core application
  // once cmdline parsing is done
  delete coreApp;



#ifdef WIN32
  //attach a console if necessary
  attachConsole();
#endif

#ifndef NO_CATCH_SIGSEGV
  // Set a handler for segfaults
  std::signal(SIGSEGV, segfaultHandling);
#endif

  OpenFlipper::Options::windowTitle(TOSTRING(PRODUCT_STRING)" v" + OpenFlipper::Options::coreVersion());

  if ( !OpenFlipper::Options::nogui() ) {

    // OpenGL check
    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    // Try creating a valid OpenGL context
    /******************************/

    // Get a valid context format
    QSurfaceFormat resultFormat = getContextFormat();

    // Set temporary(!) OpenGL settings
    OpenFlipper::Options::samples(resultFormat.samples(), true);
    OpenFlipper::Options::glStereo(resultFormat.stereo(), true);
    OpenFlipper::Options::glVersion(resultFormat.version(), true);
    OpenFlipper::Options::coreProfile(resultFormat.profile() == QSurfaceFormat::CoreProfile, true);

    // Create the actual context
    QSurfaceFormat::setDefaultFormat(resultFormat);
    QApplication app(argc, argv);
    QOffscreenSurface *surface = new QOffscreenSurface();
    surface->create();

    // Make the globally shared OpenGLContext current
    QOpenGLContext::globalShareContext()->makeCurrent(surface);
    /******************************/


    // Check whether there is OpenGL support. If not, return.
#if QT_VERSION_MAJOR < 6
    if ( !QGLFormat::hasOpenGL() ) {
#else
    if ( QOpenGLContext::openGLModuleType() != QOpenGLContext::LibGL ) {
#endif
      std::cerr << "This system has no OpenGL support.\n";
      return -1;
    }

    // create core ( this also reads the ini files )
    Core * w = new Core( );
#ifdef PYTHON_ENABLED
    setCorePointer(w);
#endif

    QString tLang = OpenFlipperSettings().value("Core/Language/Translation","en_US").toString();

    if (tLang == "locale")
      tLang = QLocale::system().name();

    // Install translator for qt internals
    QTranslator qtTranslator;
    #if QT_VERSION_MAJOR < 6
    qtTranslator.load("qt_" + tLang, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    #else
      if ( ! qtTranslator.load("qt_" + tLang, QLibraryInfo::path(QLibraryInfo::TranslationsPath)) ) {
          std::cerr << "Failed to load translation files!" << std::endl;
      }
    #endif

    app.installTranslator(&qtTranslator);
    
    // install translator for Core Application
    QString translationDir = OpenFlipper::Options::translationsDirStr() + QDir::separator();
    QDir dir(translationDir);
    dir.setFilter(QDir::Files);

    QFileInfoList list = dir.entryInfoList();

    for (int i = 0; i < list.size(); ++i) {
      QFileInfo fileInfo = list.at(i);

      if ( fileInfo.baseName().contains(tLang) ){
        QTranslator* myAppTranslator = new QTranslator();

        if ( myAppTranslator->load( fileInfo.filePath() ) )
        {    
          app.installTranslator(myAppTranslator);
        } else 
        {
  	        delete myAppTranslator;
        }
      }
     }

    #ifndef __APPLE__
    initGlew();
    #endif

    // After setting all Options from command line, build the real gui
    w->init();

    const QStringList positionalArguments = parser.positionalArguments();

    for ( auto file: positionalArguments ) {
      w->commandLineOpen(file, openPolyMeshes);
    }

    return app.exec();

  } else {

    QCoreApplication app(argc,argv);

    // create widget ( this also reads the ini files )
    Core * w = new Core( );


#ifdef PYTHON_ENABLED
    setCorePointer(w);
#endif

    // After setting all Options from command line, build the real gui
    w->init();

    const QStringList positionalArguments = parser.positionalArguments();

    for ( auto file: positionalArguments ) {
      w->commandLineOpen(file, openPolyMeshes);
    }

    return app.exec();
  }

  return 0;
}

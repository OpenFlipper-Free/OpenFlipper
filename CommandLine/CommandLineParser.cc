
#include "CommandLineParser.hh"
#include <iostream>
#include <OpenFlipper/common/GlobalOptions.hh>
#include <OpenFlipper/BasePlugin/PluginFunctionsCore.hh>


// Parse all options
CommandLineParseResult parseCommandLine(QCommandLineParser &parser, QString *errorMessage) {

  #ifndef WIN32
  #ifndef __APPLE__
    //workaround for bug with stereo mode on Qt5.7.0 and Qt5.7.1 on Linux
    int QtVersionMajor, QtVersionMinor, QtVersionPatch;
    if(sscanf(qVersion(),"%1d.%1d.%1d",&QtVersionMajor, &QtVersionMinor, &QtVersionPatch) == 3)
    {
      if(QtVersionMajor == 5 && QtVersionMinor >= 7)
      {
        if(QtVersionPatch < 2)
        {
          std::cerr << "The used Qt Version does not support stereo mode. Disabling stereo mode." << std::endl;
          OpenFlipper::Options::stereo(false);
        }
        else
          std::cerr << "Stereo Mode has not been tested for the used Qt Version." << std::endl;
      }
    }
  #endif
  #endif


 parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);


 QCommandLineOption debugOption(QStringList() << "d" << "debug",QCoreApplication::translate("main", "Enable debugging mode"));
 parser.addOption(debugOption);

 QCommandLineOption stereoOption("disable-stereo",QCoreApplication::translate("main", "Disable stereo mode"));
 parser.addOption(stereoOption);

 QCommandLineOption batchOption(QStringList() << "b" << "batch",QCoreApplication::translate("main", "Batch mode, you have to provide a script for execution"));
 parser.addOption(batchOption);

 QCommandLineOption logConsoleOption(QStringList() << "c" << "log-to-console",QCoreApplication::translate("main", "Write logger window contents to console"));
 parser.addOption(logConsoleOption);

 QCommandLineOption remoteControlOption("remote-control",QCoreApplication::translate("main", "Batch mode accepting remote connections"));
 parser.addOption(remoteControlOption);

 QCommandLineOption fulscreenOption(QStringList() << "f" << "fullscreen",QCoreApplication::translate("main", "Start in fullscreen mode"));
 parser.addOption(fulscreenOption);

 QCommandLineOption hideLoggerOption(QStringList() << "l" << "hide-logger",QCoreApplication::translate("main", "Start with hidden log window"));
 parser.addOption(hideLoggerOption);

 QCommandLineOption hideToolboxOption(QStringList() << "t" << "hide-toolbox",QCoreApplication::translate("main", "Start with hidden toolbox"));
 parser.addOption(hideToolboxOption);

 QCommandLineOption noSplashOption("no-splash",QCoreApplication::translate("main", "Hide splash screen"));
 parser.addOption(noSplashOption);

 QCommandLineOption polyMeshOption("p",QCoreApplication::translate("main", "Open files as PolyMeshes"));
 parser.addOption(polyMeshOption);

 QCommandLineOption remotePortOption("remote-port",QCoreApplication::translate("main", "Remote port"),"portnumber");
 parser.addOption(remotePortOption);

 QCommandLineOption coreProfileOption("core-profile",QCoreApplication::translate("main", "OpenGL Core Profile Mode"));
 parser.addOption(coreProfileOption);
 
 QCommandLineOption glVersionOption("glVersion",QCoreApplication::translate("main","Request OpenGL version <major>.<minor> "),QCoreApplication::translate("main","< 1.0 | 1.1 | ... | 4.6 >"));
 parser.addOption(glVersionOption);
 
 QCommandLineOption samplesOption("samples",QCoreApplication::translate("main","Overwrite multisampling sample count"),QCoreApplication::translate("main","< 0 | 1 | 2 | ... | 16 >"));
 parser.addOption(samplesOption);
 
 QCommandLineOption glStereoOption("glStereo",QCoreApplication::translate("main","Overwrite OpenGL Stereo setting"),QCoreApplication::translate("main","< true | false >"));
 parser.addOption(glStereoOption);
 
 QCommandLineOption profileOption("profile",QCoreApplication::translate("main","Request OpenGL context profile <profile> with profile set as compat or core"),QCoreApplication::translate("main","< compat | core >"));
 parser.addOption(profileOption);

 QCommandLineOption pluginOptionsOption(QStringList() << "o" << "pluginoptions",QCoreApplication::translate("main", "Pass options to plugins"), "key1=value1;key2=value2;...");
 parser.addOption(pluginOptionsOption);

 const QCommandLineOption helpOption = parser.addHelpOption();
 const QCommandLineOption versionOption = parser.addVersionOption();


 // Now parse the command line
 if (!parser.parse(QCoreApplication::arguments())) {
   *errorMessage = parser.errorText();
   return CommandLineError;
 }

 if (parser.isSet(helpOption))
   return CommandLineHelpRequested;

 if (parser.isSet(versionOption))
   return CommandLineVersionRequested;

 if (parser.isSet(debugOption)) {
   OpenFlipper::Options::debug(true);
 }

 if (parser.isSet(stereoOption)) {
   OpenFlipper::Options::stereo(false);
 }

 if (parser.isSet(batchOption)) {
   OpenFlipper::Options::nogui(true);
 }

 if (parser.isSet(logConsoleOption)) {
   OpenFlipper::Options::logToConsole(true);
 }

 if (parser.isSet(remoteControlOption)) {
   OpenFlipper::Options::remoteControl(true);
 }

 if (parser.isSet(fulscreenOption)) {
   OpenFlipperSettings().setValue("Core/Gui/fullscreen",true);
 }

 if (parser.isSet(hideLoggerOption)) {
   OpenFlipper::Options::loggerState(OpenFlipper::Options::Hidden);
 }

 if (parser.isSet(hideToolboxOption)) {
   OpenFlipperSettings().setValue("Core/Gui/ToolBoxes/hidden",true);
 }

 if (parser.isSet(noSplashOption)) {
   OpenFlipperSettings().setValue("Core/Gui/splash",false);
 }

 if (parser.isSet(polyMeshOption)) {
   openPolyMeshes = true;
 }

 if (parser.isSet(remotePortOption)) {
   const QString port = parser.value("remote-port");
   std::cerr << "Got port option : " << port.toStdString() << std::endl;
   OpenFlipper::Options::remoteControl(port.toInt());
 }
 if(parser.isSet("samples"))
   OpenFlipper::Options::samples(parser.value("samples").toInt(),true);
 if(parser.isSet("glVersion"))
 {
   QStringList values = parser.value("glVersion").split(".");
   QPair<int,int> version(
          values[0].toInt(),
          values[1].toInt());
   OpenFlipper::Options::glVersion(version,true);
 }
 
 if(parser.isSet("glStereo"))
   OpenFlipper::Options::glStereo(parser.value("glStereo")=="true");

 if(parser.value(profileOption)=="core")
 {
     OpenFlipper::Options::coreProfile(true, true);
 }
 else
 {
     if(parser.value(profileOption)=="compat")
     {
         OpenFlipper::Options::coreProfile(false, true);
     }
 }
 if(parser.isSet(coreProfileOption)) {
   OpenFlipper::Options::coreProfile(true, true);
 }
 if(parser.isSet(pluginOptionsOption))
 {
   QStringList poptions = parser.value(pluginOptionsOption).split(";");
   QVector<QPair<QString, QString>> pcloptions;
   for(auto s : poptions)
   {
       auto kvp = s.split("=");

       // Only consider terms of the kind "key=value"
       if(kvp.size() != 2u)
           continue;

       auto key = kvp[0];
       auto value = kvp[1];
       pcloptions.push_back({key, value});
   }
   PluginFunctions::setPluginCommandLineOptions(pcloptions);
 }

 return CommandLineOk;
}

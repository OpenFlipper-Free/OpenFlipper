
#include <QCommandLineParser>

enum CommandLineParseResult
{
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested
};

static bool openPolyMeshes = false;
static bool remoteControl  = false;

// Parse all options
CommandLineParseResult parseCommandLine(QCommandLineParser &parser, QString *errorMessage);

/*===========================================================================*\
*                                                                            *
 *                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                            *
 \*===========================================================================*/

#include "RAMInfo.hh"

#include <QString>
#include <stdio.h>

// Main Memory information
#ifdef WIN32
#include <Windows.h>
#elif defined ARCH_DARWIN
#include <mach/mach_host.h>
#include <mach/vm_statistics.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#endif
// private struct to get ram information
namespace{
struct MemoryVacancy{
  uint64_t totalRamMB;
  uint64_t freeRamMB;
  uint64_t bufferRamMB;
};

void parseMeminfo(uint64_t& _total, uint64_t& _free, uint64_t& _buffer)
{
  //use the specific type for scanf so we have at least 64 bit to read memory information
  //this way we support memory information for max 9,223,372,036,854,775,807 byte of memory (that is 8 exa byte)
  unsigned long long memcache, memfree, total, free, buffer;

  FILE* info = fopen("/proc/meminfo","r");
  if(fscanf (info, "MemTotal: %19llu kB MemFree: %19llu kB Buffers: %19llu kB Cached: %19llu kB",&total, &memfree, &buffer, &memcache) < 4) //try to parse the old meminfo format
  {
    fclose(info);
    info = fopen("/proc/meminfo","r");
    //parsing the old format failed so we try to parse using the new format
    if(fscanf(info, "MemTotal: %19llu kB MemFree: %19llu kB MemAvailable: %19llu kB Buffers: %19llu kB Cached: %19llu kB",&total, &memfree, &free, &buffer, &memcache) < 5)
    {
      //parsing failed overall so return 0 for all values
      total = 0;
      free = 0;
      buffer = 0;
    }
    else
    {
      free = memfree + (buffer + memcache);  //everything is fine
    }

  }
  else  //compute available memory
  {
    free = memfree + (buffer + memcache);
  }
  fclose(info);
  _total = total;
  _free = free;
  _buffer = buffer;
}

}
namespace Utils
{
  namespace Memory
  {
    void MemoryInfoUpdate(MemoryVacancy & _outMemoryVacancy) {

      //initialize to 0 just in case something fails or cant be read
      _outMemoryVacancy.totalRamMB = 0;
      _outMemoryVacancy.freeRamMB = 0;
      _outMemoryVacancy.bufferRamMB = 0;

      // Main Memory information
    #ifdef WIN32 //Windows
      // Define memory structure
      MEMORYSTATUSEX ms;
      // Set the size ( required according to spec ... why???? )
      ms.dwLength = sizeof(ms);
      // Get the info
      GlobalMemoryStatusEx(&ms);

      _outMemoryVacancy.totalRamMB = ms.ullTotalPhys / 1024 / 1024;
      _outMemoryVacancy.freeRamMB = ms.ullAvailPhys / 1024 / 1024;

    #elif defined ARCH_DARWIN // Apple (sadly cant query free memory)
      mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
      vm_statistics_data_t vmstat;
      if(KERN_SUCCESS != host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmstat, &count))
      {  // An error occurred
      }
      else
      {
          //retrieve real physical memory
          int mib[2];
          uint64_t physical_memory;
          size_t length;
          mib[0] = CTL_HW;
          mib[1] = HW_MEMSIZE;
          length = sizeof(uint64_t);
          if( sysctl(mib, 2, &physical_memory, &length, NULL, 0) == -1 )
          {
              physical_memory = 0;
          }
          // retrieve free memory
          _outMemoryVacancy.totalRamMB = physical_memory / 1024 / 1024;
          unsigned long active = vmstat.active_count * PAGE_SIZE / 1024 / 1024;
          _outMemoryVacancy.freeRamMB =  _outMemoryVacancy.totalRamMB - active;
          _outMemoryVacancy.bufferRamMB =0;
      }

    #else // Linux

      uint64_t total, free, buffer;
      parseMeminfo(total, free, buffer);

      // Unit in kbytes ; /1024 -> MB
      _outMemoryVacancy.totalRamMB = total / 1024;
      _outMemoryVacancy.freeRamMB = free / 1024;
      _outMemoryVacancy.bufferRamMB = buffer / 1024; // Buffers get freed, if we don't have enough free ram
    #endif
    }

   uint64_t queryFreeRAM()
   {
     MemoryVacancy vac;
     MemoryInfoUpdate(vac);
     return vac.freeRamMB;
   }

   uint64_t queryTotalRAM()
   {
     MemoryVacancy vac;
     MemoryInfoUpdate(vac);
     return vac.totalRamMB;
   }

   uint64_t queryBufferedRAM()
   {
     MemoryVacancy vac;
     MemoryInfoUpdate(vac);
     return vac.bufferRamMB;
   }
  }
}

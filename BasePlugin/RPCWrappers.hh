/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


/**
 * \file RPCWrappers.hh
 * This file contains functions to call functions and procedures across plugins.
 * The QT Scripting system is used to pass the calls between different plugins.
 *
 * Usage is described in \ref RPCInterfacePage
 */

#pragma once


#include <vector>
#include <QVariant>
#include <OpenFlipper/common/Types.hh>


/** Namespace containing RPC helper functions used to call functions across plugins
 *
 * Usage is described in \ref RPCInterfacePage
 */
namespace RPC {

//===========================================================================
/** @name Call functions across plugins (simple calls)
 *
 * These functions can be used to call functions in other plugins.
 * @{ */
//===========================================================================

/** \brief Call a function provided by a plugin getting multiple parameters as a vector of qvariants
 *
 * This function will be used to call functions in other plugins and the core.
 *
 * @param _plugin Plugin name ( Scripting name of the plugin )
 * @param _functionName Name of the remote function
 * @param _returnArg Possibl return argument. Can be empty ( use default constructor to generate it
 * @param _parameters vector of QVariants containing the functions parameters in the right order
 */
DLLEXPORT
void callFunctionQVariant( const QString& _plugin, const QString& _functionName , const std::vector< QVariant >& _parameters , QGenericReturnArgument _returnArg = QGenericReturnArgument());

/** @} */

//===========================================================================
/** @name Call functions across plugins
 *
 * These templates can be used to call functions in other plugins.
 * @{ */
//===========================================================================


/** \brief call a function in another plugin
 *
 * @param _plugin       Plugin name ( Scripting name of the plugin )
 * @param _functionName Name of the remote function
 * @param args          Variable number of call arguments
 */
DLLEXPORT
void callFunction( QString _plugin, QString _functionName);

template <class... Ts>
void callFunction(QString _plugin, QString _functionName, Ts const&... args) {


    QGenericReturnArgument unused;
    std::vector<QVariant> parameters( { QVariant::fromValue( args ) ... } );

    callFunctionQVariant(_plugin,_functionName,parameters);
}


/** @} */

//===========================================================================
/** @name Call functions across plugins which return a value
 *
 * These templates can be used to call functions that return a value.
 * You have to pass the type of return value as the first template parameter.
  * @{ */
//===========================================================================

/** \brief call a function in another plugin and get a return parameter
 *
 * @param _plugin       Plugin name ( Scripting name of the plugin )
 * @param _functionName Name of the remote function
 * @return value returned by the called function
 */
template <typename ReturnValue >
ReturnValue callFunctionValue( QString _plugin, QString _functionName) {

    // Intermediate storage for the return value as we have to pass it through the GenericReturnargument to hide the type
    ReturnValue tmpValue = 0;

    // Two variants of getting the type mapping for the Returnvalue:
    // 1. via qvariant (which is rather ugly
    //   QVariant::fromValue<ReturnValue>(tmpValue).typeName()
    // 2. via the metatype system (Does not seem to work!)
    //   QMetaType::fromType<ReturnValue>().name().data()

    // Return argument encapsulation for the final call to the function
    QGenericReturnArgument argument1 = QGenericReturnArgument(QVariant::fromValue<ReturnValue>(tmpValue).typeName(),&tmpValue);

    std::vector<QVariant> parameters;

    callFunctionQVariant(_plugin,_functionName,parameters,argument1);

    return tmpValue;
}

/** \brief call a function in another plugin and get a return parameter
 *
 * @param _plugin       Plugin name ( Scripting name of the plugin )
 * @param _functionName Name of the remote function
 * @param args          Variable number of call arguments
 * @return value returned by the called function
 */
template <typename ReturnValue ,class... Ts>
ReturnValue callFunctionValue(QString _plugin, QString _functionName, Ts const&... args) {

    // Intermediate storage for the return value as we have to pass it through the GenericReturnargument to hide the type
    ReturnValue tmpValue = 0;

    // Return argument encapsulation for the final call to the function
    QGenericReturnArgument argument1 = QGenericReturnArgument(QVariant::fromValue<ReturnValue>(tmpValue).typeName(),&tmpValue);

    std::vector<QVariant> parameters( { QVariant::fromValue( args ) ... } );

    callFunctionQVariant(_plugin,_functionName,parameters,argument1);

    return tmpValue;
}

/** @} */


}


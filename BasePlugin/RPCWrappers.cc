/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#include <OpenFlipper/BasePlugin/RPCWrappers.hh>
#include <OpenFlipper/common/PluginStorage.hh>
//#include "RPCWrappersHelper.hh"

#include <QMetaType>

#include <QApplication>

namespace RPC {


void callFunctionQVariant( const QString& _plugin, const QString& _functionName , const std::vector< QVariant >& _parameters , QGenericReturnArgument _returnArg) {

    if (_plugin.toLower() == "core" ) {
        std::cerr << "callFunctionReturnQVariant : Core not supported! Use Pluginfunctions to trigger core functionality!!!" << std::endl;
        return;
    }

    std::cerr << "Calling function: " << _plugin.toStdString() << " " << _functionName.toStdString() <<  std::endl;

    // @TODO, Check the correct thread affinity for the new style function calls
    // If we get into trouble here, check the old RPCWrappersHelper.

    //    RPCHelper h;

    //    Qt::ConnectionType connection = Qt::DirectConnection;
    //    if (h.thread() != QApplication::instance()->thread())
    //    {
    //      h.moveToThread(QApplication::instance()->thread());
    //      connection = Qt::BlockingQueuedConnection;
    //    }

    //    QScriptValue retVal;
    //    if (!QMetaType::type("std::vector<QScriptValue>"))
    //      qRegisterMetaType< std::vector< QScriptValue > >("ScriptParameters");

    //call _functionName in main thread. blocks, if our function runs in another thread


    // Get a list of all the plugins
    const std::vector<PluginInfo>& plugins = PluginStorage::plugins();

    QObject* plugin = nullptr;

    for ( auto current : plugins ) {
        if (current.rpcName == _plugin) {
            plugin = current.plugin;
            break;
        }
    }

    if ( plugin == nullptr ) {
        std::cerr << "Unable to find plugin " << _plugin.toStdString() << " in callFunctionQVariant"<< std::endl;
        return;
    }


    QGenericArgument arguments[10];

    // Unpack the arguments from the vector
    for ( size_t i = 0 ; i < _parameters.size() ; ++i ) {
        arguments[i] = QGenericArgument(_parameters[i].typeName(),_parameters[i].data());
    }

    // Add empty arguments for the rest
    for (size_t i = _parameters.size() ; i < 10; ++i ) {
        arguments[i] = QGenericArgument();
    }



    QMetaObject::invokeMethod(plugin, _functionName.toStdString().c_str(), Qt::DirectConnection, _returnArg,
                              arguments[0],
            arguments[1],
            arguments[2],
            arguments[3],
            arguments[4],
            arguments[5],
            arguments[6],
            arguments[7],
            arguments[8],
            arguments[9]
            );

}

void callFunction( QString _plugin, QString _functionName) {
    QGenericReturnArgument unused;
    std::vector<QVariant> parameters;
    callFunctionQVariant(_plugin,_functionName,parameters);
}

}


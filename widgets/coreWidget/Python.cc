#include "CoreWidget.hh"

void CoreWidget::showPythonScriptInterpreter() {

  if ( OpenFlipper::Options::nogui() )
     return;

  if (   pythonWidget_ == nullptr )
    pythonWidget_ = new PythonWidget( this );

   pythonWidget_->show();

}
